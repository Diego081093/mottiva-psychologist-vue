// IMPORTS OF PAGES

// CREATE ROUTES AND VALIDATIONS
const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: { name: 'LoginForm' }
  },
  {
    path: '/login',
    // IMPORT LAYOUT  ==> DEFAULT
    component: () => import('../components/06-layouts/default.vue'),
    children: [
      {
        path: '',
        name: 'LoginForm',
        component: () => import('../components/05-pages/login/default.vue'),
        meta: {
          guest: true,
          title: 'Ingresar'
        }
      }
    ]
  },
  {
    path: '/register',
    // IMPORT LAYOUT  ==> DEFAULT
    component: () => import('../components/06-layouts/default.vue'),
    children: [
      {
        path: '',
        component: () => import('../components/05-pages/register/default.vue'),
        meta: {
          guest: true,
          title: 'Registrar'
        }
      }
    ]
  },
  {
    path: '/dashboard',
    // IMPORT LAYOUT  ==> DASHBOARD
    component: () => import('../components/06-layouts/dashboard.vue'),
    children: [
      {
        path: '',
        redirect: { name: 'DiarySessionsPage' },
        meta: { requiresAuth: true }
      },
      {
        path: '/agenda',
        component: () => import('../components/04-templates/diary/default.vue'),
        children: [
          {
            path: '',
            redirect: { name: 'DiarySessionsPage' },
            meta: { requiresAuth: true }
          },
          {
            path: 'sesiones',
            name: 'DiarySessionsPage',
            component: () => import('../components/05-pages/sessions/default.vue'),
            meta: {
              requiresAuth: true,
              title: 'Agenda'
            }
          },
          {
            path: 'notas',
            name: 'DiaryNotesPage',
            component: () => import('../components/05-pages/notes/default.vue'),
            meta: {
              requiresAuth: true,
              title: 'Agenda'
            }
          },
          {
            path: 'capacitaciones',
            name: 'DiaryTrainingsPage',
            component: () => import('../components/05-pages/trainings/default.vue'),
            meta: {
              requiresAuth: true,
              title: 'Agenda'
            }
          },
          {
            path: 'capacitaciones/:slug',
            name: 'TrainingDetail',
            component: () => import('../components/05-pages/trainings/detail.vue'),
            meta: {
              requiresAuth: true,
              title: 'Capacitación'
            }
          },
          {
            path: 'notificaciones',
            name: 'DiaryNotificationsPage',
            component: () => import('../components/05-pages/notifications/default.vue'),
            meta: {
              requiresAuth: true,
              title: 'Agenda'
            }
          }
        ]
      },
      {
        path: '/historial',
        component: () => import('../components/04-templates/history/default.vue'),
        children: [
          {
            path: '',
            redirect: { name: 'HistorySessionsPage' },
            meta: { requiresAuth: true }
          },
          {
            path: 'sesiones',
            name: 'HistorySessionsPage',
            component: () => import('../components/05-pages/sessions/history.vue'),
            meta: {
              requiresAuth: true,
              title: 'Sesiones'
            }
          },
          {
            path: 'sala-de-emociones',
            name: 'HistoryEmotionPage',
            component: () => import('../components/05-pages/emotionRoom/default.vue'),
            meta: {
              requiresAuth: true,
              title: 'Agenda'
            }
          },
          {
            path: 'pacientes',
            name: 'HistoryPatientPage',
            component: () => import('../components/05-pages/patients/history.vue'),
            meta: {
              requiresAuth: true,
              title: 'Agenda'
            }
          }
        ]
      },
      {
        path: '/mottivateca',
        component: () => import('../components/04-templates/mottivateca/default.vue'),
        children: [
          {
            path: '',
            redirect: { name: 'MottivatecaForYouPage' },
            meta: { requiresAuth: true }
          },
          {
            path: 'para-mi',
            name: 'MottivatecaForYouPage',
            component: () => import('../components/05-pages/mottivateca/default.vue'),
            meta: {
              requiresAuth: true,
              title: 'Para mi'
            }
          },
          {
            path: 'para-pacientes',
            name: 'MottivatecaPatientsPage',
            component: () => import('../components/05-pages/mottivateca/patients.vue'),
            meta: {
              requiresAuth: true,
              title: 'Para pacientes'
            }
          },
          {
            path: ':slug',
            name: 'MottivatecaDetail',
            component: () => import('../components/05-pages/mottivateca/detail.vue'),
            meta: {
              requiresAuth: true,
              title: 'Mottivateca'
            }
          }
        ]
      }
    ]
  },
  {
    path: '/layout',
    // IMPORT LAYOUT  ==> DASHBOARD
    component: () => import('../components/06-layouts/dashboard-clean.vue'),
    children: [
      {
        path: '/perfil',
        name: 'ProfilePage',
        component: () => import('../components/05-pages/profile/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Perfil'
        }
      },
      {
        path: '/perfil/editar',
        name: 'ProfileEditPage',
        component: () => import('../components/05-pages/profile/edit.vue'),
        meta: {
          requiresAuth: true,
          title: 'Editar perfil'
        }
      },
      {
        path: '/preguntas-frecuentes',
        name: 'FrequentQuestionsPage',
        component: () => import('../components/05-pages/frequentQuestions/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Preguntas Frecuentes'
        }
      },
      {
        path: '/sala-de-emociones/:slug',
        name: 'EmotionRoomDetail',
        component: () => import('../components/05-pages/emotionRoom/detail.vue'),
        meta: {
          requiresAuth: true,
          title: 'Sala de emocion'
        }
      },
      {
        path: '/terapia/:slug',
        name: 'TherapyDetail',
        component: () => import('../components/05-pages/therapy/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Terapia'
        }
      },
      {
        path: '/notas/:slug',
        name: 'NotesDetail',
        component: () => import('../components/05-pages/notes/detail.vue'),
        meta: {
          requiresAuth: true,
          title: 'Terapia'
        }
      }
    ]
  }
]

export default routes
