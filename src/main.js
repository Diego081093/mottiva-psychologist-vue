import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { io } from 'socket.io-client'
import VueSocketIO from 'vue-socket.io'
import endpoint from './plugins/endpoint'

/**
 * -----------------------------------------
 *  Import Libraries for project
 * -----------------------------------------
 */
import './plugins/veeValidate'
import './plugins/vueCookies'
import './plugins/vueJsModal'
import './plugins/vueMoment'
import './plugins/vueDatePicker'

Vue.use(
  new VueSocketIO({
    debug: true,
    connection: io(endpoint.SOCKET)
  })
)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
