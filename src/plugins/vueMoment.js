import Vue from 'vue'
import moment from 'moment-timezone'
import VueMoment from 'vue-moment'

require('moment/locale/es')

moment.locale('es')
moment.tz.setDefault('UTC')

Vue.use(VueMoment, { moment })
