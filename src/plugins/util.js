export function playSound () {
  const audio = new Audio('https://api.mottiva.pe/tukutun.mp3')
  audio.play().then(r => {
    setTimeout(function () {
      audio.pause()
    }, 1000)
  })
}
