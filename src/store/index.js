import Vue from 'vue'
import Vuex from 'vuex'
// IMPORT plugins
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// import of modules
import Profile from './Profile/index'
import Auth from './Auth/index'
import Sessions from './Sessions/index'
import Notes from './Notes/index'
import Trainings from './Trainings/index'
import Rooms from './Rooms/index'
import Patients from './Patients/index'
import Tasks from './Tasks/index'
import Diary from './Diary/index'
import Specialty from './Specialty/index'
import Notification from './Notification/index'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Auth,
    Profile,
    Sessions,
    Notes,
    Trainings,
    Rooms,
    Patients,
    Tasks,
    Diary,
    Specialty,
    Notification
  }
})
/**
 * THIS FORMAT NAMED FUNCTIONS :
 * State      -> S_
 * Mutation   -> M_
 * Action     -> A_
 * Getter     -> G_
 *
 * FORMAT FUNCTION LOGICS
 * SET      -> setting data in state
 * PUSH     -> pushing data in array state
 *
 * DEFAULT MUTATIONS
 * SET_DATA     -> set data in element state
 * PUSH_DATA    -> push data in element state
 */
