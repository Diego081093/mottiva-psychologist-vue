// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_PROFILES: null,
  S_PROFILE: {}
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_ATTRIBUTE ({ commit }, { id, params }) {
    try {
      const { data } = await HTTP.get(`profileAttribute/${id}`, params)
      return data
    } catch (error) {
      console.log('ERROR_GET_ATTRIBUTE:', error)
      throw error
    }
  },
  async A_SET_PROFILE ({ commit }, body) {
    try {
      const { data } = await HTTP.put('psycology', body)
      return data
    } catch (error) {
      console.log('ERROR_SET_PROFILE [ACTION]', error)
    }
  },
  async A_SET_PHOTO ({ commit }, body) {
    try {
      const { data } = await HTTP.put('psycology/photo', body)
      return data
    } catch (error) {
      console.log('ERROR_SET_PROFILE [ACTION]', error)
    }
  },
  async A_SET_ATTRIBUTE ({ commit }, { id, body }) {
    try {
      const { data } = await HTTP.post(`profileAttribute/${id}`, body)
      return data
    } catch (error) {
      console.log('ERROR_SET_ATTRIBUTE:', error)
      throw error
    }
  },
  async A_GET_CHAT_HELPDESK ({ commit }) {
    try {
      const { data } = await HTTP.get('chat/helpdesk')
      return data
    } catch (error) {
      console.log('ERROR_GET_CHAT_HELPDESK:', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
