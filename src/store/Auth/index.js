// IMPORT LIBRARIES
import Vue from 'vue'
import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_PROFILE: {},
  S_AUTH: null,
  S_TWILIO_AUTH: null
}
const getters = {
  isAuthenticated () {
    return !!Vue.$cookies.get('AUTH_TOKEN')
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  M_SET_USER (state, username) {
    state.user = username
  },
  M_LOGOUT (state) {
    state.user = null
    state.posts = null
  }
}
const actions = {
  async A_REGISTER ({ dispatch }, form) {
    try {
      await HTTP.post('register', form)
      await dispatch('A_LOGIN', {
        username: form.username,
        password: form.password
      })
    } catch (error) {
      console.log('ERROR_REGISTER [ACTION]', error)
    }
  },
  async A_LOGIN ({ commit }, User) {
    try {
      const { data } = await HTTP.post('login', User)
      Vue.$cookies.set('AUTH_TOKEN', data.data.token)
      console.log('jwt decode', jwtDecode(data.data.token))

      commit('SET_DATA', {
        destination: 'S_AUTH',
        data: data.data.token
      })
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  },
  async A_LOGOUT ({ commit }) {
    const user = null
    Vue.$cookies.remove('AUTH_TOKEN')
    commit('SET_DATA', {
      destination: 'S_AUTH',
      data: user
    })
  },
  async A_GET_PROFILE ({ commit }) {
    try {
      const { data } = await HTTP.get('profile/me')
      commit('SET_DATA', {
        destination: 'S_PROFILE',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_PROFILE [ACTION]', error)
    }
  },
  async A_TWILLIO_ACCESS ({ commit }, params) {
    try {
      const { data } = await HTTP.post('twilio/accessToken', params)
      commit('SET_DATA', {
        destination: 'S_TWILIO_AUTH',
        data: data.data.token
      })
      return data.data.token
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
