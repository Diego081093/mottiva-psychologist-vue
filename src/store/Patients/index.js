// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_PATIENTS: []
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_PATIENTS ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`patient-by?to=${Params.to}`)

      commit('SET_DATA', {
        destination: 'S_PATIENTS',
        data: data.patients
      })
      return data
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
