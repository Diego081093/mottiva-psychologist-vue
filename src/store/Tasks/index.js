// IMPORT LIBRARIES
import Vue from 'vue'

import { HTTP } from '@/plugins/axios'

const state = {
  S_TASKS: null,
  S_TASK: {}
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_TASKSPATIENT ({ commit }, { params }) {
    try {
      const { data } = await HTTP.get('task/patient', { params })
      commit('SET_DATA', {
        destination: 'S_TASKS',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_TASKSPATIENT [ACTION]', error)
      throw error
    }
  },
  async A_SET_SESSIONTASK ({ commit }, { body }) {
    try {
      const { data } = await HTTP.post('task/sessionTask', body)
      return data
    } catch (error) {
      console.log('ERROR_SET_SESSIONTASK:', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
