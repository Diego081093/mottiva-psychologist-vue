// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_TRAININGS: [],
  S_TRAINING: {},
  S_FILTERS: {}
}
const getters = {
  G_SEENS: (state) => (position) => {
    return state.S_TRAININGS.filter(training => parseInt('0' + training.done) > position)
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_TRAININGS ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`trainings?role=${Params.role}&type_cat_media=${Params.type_cat_media}&type=${Params.type}&category=${Params.category}&favorite=${Params.favorite}&archived=${Params.archived}`)

      commit('SET_DATA', {
        destination: 'S_TRAININGS',
        data: data.data
      })
      return data
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      commit('SET_DATA', {
        destination: 'S_TRAININGS',
        data: []
      })
      throw error
    }
  },
  async A_GET_FILTERS ({ commit }) {
    try {
      const { data } = await HTTP.get('trainings/filters')

      commit('SET_DATA', {
        destination: 'S_FILTERS',
        data: data.filters
      })
      return data
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  },
  async A_GET_TRAINING ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`trainings/${Params}`)

      commit('SET_DATA', {
        destination: 'S_TRAINING',
        data: data.data
      })
      return data
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  },
  async A_UPDATE_FAVORITE ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`favorite/${Params.id}`)

      // commit('SET_DATA', {
      //   destination: 'S_TRAINING',
      //   data: data.data
      // })
      return data
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
