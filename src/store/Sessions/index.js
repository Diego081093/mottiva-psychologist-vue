// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_SESSIONS: null,
  S_NOTE: null,
  S_SESSION: {},
  S_CHAT: {},
  S_NOTIFY: {}
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_SESSIONS ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`sessions?offset=${Params.offset}&limit=${Params.limit}&to=${Params.to}`)

      commit('SET_DATA', {
        destination: 'S_SESSIONS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  },

  async A_SAVE_NOTE ({ commit }, params) {
    try {
      const { data } = await HTTP.post('sessions/' + params.idSession + '/notes', {
        note: params.note,
        priority: params.priority
      })
      commit('SET_DATA', {
        destination: 'S_NOTE',
        data: data
      })
    } catch (error) {
      console.log('ERROR_SAVE_NOTE [ACTION]', error)
      throw error
    }
  },
  async A_GET_SESSION ({ commit }, { id, params }) {
    try {
      const { data } = await HTTP.get(`sessions/${id}`, { params })
      commit('SET_DATA', {
        destination: 'S_SESSION',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_SESSION:', error)
      throw error
    }
  },
  async A_SET_ATTRIBUTE ({ commit }, { id, body }) {
    try {
      const { data } = await HTTP.post(`sessionsAttribute/${id}`, body)
      return data
    } catch (error) {
      console.log('Something a went error:', error)
      throw error
    }
  },
  async A_GET_SESSION_CHAT ({ commit }, id) {
    try {
      const { data } = await HTTP.get(`chat/${id}`)
      commit('SET_DATA', {
        destination: 'S_CHAT',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_SESSION:', error)
      throw error
    }
  },
  async A_GET_NOTIFY_START_SESSION ({ commit }, id) {
    try {
      const { data } = await HTTP.get(`notification/sendPushNotificationUser/${id}`)
      commit('SET_DATA', {
        destination: 'S_NOTIFY',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_SESSION:', error)
      throw error
    }
  },
  async A_SET_MESSAGE ({ commit }, body) {
    try {
      const { data } = await HTTP.post('chat/message', body)
      return data
    } catch (error) {
      console.log('Something a went error:', error)
      throw error
    }
  },
  async A_SET_NEXT_SESSION ({ commit }, params) {
    try {
      const { data } = await HTTP.post(`sessions/${params.idPatient}/next`, params)
      return data
    } catch (error) {
      console.log('ERROR_SAVE_NOTE [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
